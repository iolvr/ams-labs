package ams.logfacade;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ico
 */
public class RoomController {
    
     final Logger logger = LoggerFactory.getLogger(RoomController.class);
    static final int ALARM_LEVEL = 30;

    private float currentTemp;
    private float previousTemp;

    public void setTemperature(float temperature) {

        previousTemp = currentTemp;

        currentTemp = temperature;
        logger.debug("Temperature set to {}. Old temperature was {}.", currentTemp, previousTemp);


        if( currentTemp > ALARM_LEVEL ){
            logger.warn("Temperature has risen above {} degrees.", ALARM_LEVEL);
        }

    }
    
}
